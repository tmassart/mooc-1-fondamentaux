# M4.3 Réseaux - Chap0 - Introduction aux réseaux

[//]: <> (Vous pouvez ajouter vos commentaires de cette manière)

Le Module 3 consacré à Ethernet/TCP/IP et dans une moindre mesure aux autres réseaux commence par un chapitre d'introduction.

**diapo 2**

[//]: <> (Anthony : Je pense que ce serait bien que Josiane fasse l'introduction avec moi, vue qu'elle fait la dernière partie)

L'objectif de ce chapitre 0 est de situer Ethernet TCP/IP dans le contexte général des réseaux.

* Pour cela, on y présentera différents réseaux répondants aux besoins de différents usages.
* Suivront des notions générales et le vocabulaire utilisés dans ce domaine, 
* En insistant sur les avantages des transmissions numériques.
* Quelques exercices permettront de réviser les pré-requis nécessaires au module.
* Enfin, une introduction plus précise à Ethernet/TCP/IP précédera
* une présentation rapide des couches du modèle OSI.

## 1. Différents réseaux pour différents usages
**diapo 3**

On peut définir un réseau comme l'interconnexion de plusieurs machines entre elles, avec comme objectif le partage d'informations ou le partage de ressources.
Observons quelques exemples :

* Internet est un réseau sur lequel on trouve des ordinateurs, des smartphones accédant aux services de différents équipements : 

    - Des serveurs Web fournissent des informations (MeteoFrance),
    - Des caméras IP fournissent des images, (https://www.ville-saint-malo.fr/webcams/). Le débit nécessaire est très important.
    - Des automates industriels fournissent des données issues de la production. La fiabilité du réseau, la possibilité de superviser à distance via un téléphone ou un PC portable, sont des éléments déterminants.
    - Des serveurs info-nuagique (cloud) fournissent des logiciels, des ressources de stockage, d'analyse de données, (ovh, Microsoft Azure, Google Drive). Pour les logiciels, la latence est l'élément clé pour un fonctionnement fluide. Pour le stockage, des exigences sur le débit peuvent apparaître.
    - Des supercalculateurs fournissent des ressources de calcul (Jean Zay). Les réseaux entre les différents noeuds de calcul doivent avoir un temps de latence très faible et un débit très important.

**diapo 4**
* Dans une voiture, une trentaine de calculateurs sont connectés sur plusieurs réseaux, notamment un réseau critique pour le châssis et le moteur et un réseau pour le confort. Pour le premier, le déterminisme (temps de latence connu pour l'acheminement des message) est primordial.
* Les réseaux d'objets connectés ouvrent la voie à de nombreuses applications nouvelles. par exemple, la santé et la localisation de 45 rhinocéros du Zimbabwe sont supervisées via un réseau Sigfox.
L'autonomie des capteurs et la portée du réseau sont des éléments essentiels ici.

**diapo 5 Problématique des réseaux**

Ces 3 exemples montrent que les réseaux ont la même fonction (transmettre des informations) mais des applications très différentes. Les différentes problématiques liées aux réseaux auront donc un poids différents suivant l'application. Les principales problématiques liées à la transmission d'information sont :
* Le débit est le point clé du développement actuel du streaming vidéo HD, avec jusqu'à 1Gbit/s pour les offres fibres. En domotique ou pour le réseaux de confort d'une voiture, des débits beaucoup plus faibles (de l'ordre de 100 kbit/s) sont suffisants.
* La latence est la durée qui s'écoule entre la demande d'envoi d'un message et sa réception. En téléphonie, une communication internationale est acceptable si la latence est inférieure à 300 ms.
* Le déterminisme est la possibilité sur un réseau de garantir ce temps de latence. C'est un élément primordial pour les systèmes de contrôle-commande (la synchronisation des différents axes d'un robot demande de garantir une latence inférieur à quelques µs. Pour la première opération chirugicale à distance en 2001, Orange avait garanti 50 ms de latence.)
* La fiabilité consiste à acheminer sans erreur des messages. Celle-ci est primoridale sur les systèmes critiques (contrôle commande d'un avion par exemple), beaucoup moins pour le streaming de fichiers audio.
* La robustesse se traduit par un faible taux de pannes. Pour les systèmes très critiques (avion, centrale nucléaire) la robustesse est améliorée via la redondance des réseaux.
* La sécurité regroupe l'authentification des acteurs de la communication, la confidentialité des échanges et leur intégrité (les messages ne peuvent être modifiés). La sécurité est indispensable aux échanges bancaires, pas à la diffusion de la TNT.
* La consommation nécessaire pour dialoguer sur ces réseaux est liée aux débit et au chiffrement imposé notamment. Les réesaux IoT pour objets connectés autonomes sont très attentifs à celle-ci. Le chahier des charges 2018 de Veolia pour les compteurs d'eau demandait 20 ans d'autonomie à l'émetteur, avec une pile.
* Le coût prend en compte évidemment le coût des équipements terminaux et des équipements d'infrastructure mais aussi celui des abonnement. Les réseaux cellulaires utilisent des bandes de fréquences achetées très cher par les opérateurs alors que les réseaux IoT utilisent pour la plupart la bande de fréqeunce gratuite ISM à 868 MHz.
* Enfin, l'encombrement des circuits est à prendre en compte pour la conception des smartphones ou des objets connectés.


**diapo 6 Réseau local / réseau étendu**

LAN : Local Area Network
La ou les personnes en charge de la mise en place et de la gestion du réseau local d'une entreise sont les administrateurs réseaux, faisant partie de la direction des services informatiques.

WAN : Wide Area Netowrk
Le réseau cellulaire utilise désormais le réseau internet, y compris pour la voix.

**diapo 7 Réseau local / réseau étendu : PAN et MAN**

Parmi ces réseaux LAN, on trouve d'autres dénominations plus précises.
PAN : Personal Area Network

Dans une maison équipée avec un réseau sépcifique domotique, le réseau associant les interrupteurs, capteurs de présence, luminaires, volets et prises connectées peut entrer dans la catégorie PAN.

Ces réseaux se caractérisent par un faible débit, une faible portée et une très faible consommation.

MAN : Metropolitan Area Network

Si l'infrastructure du réseau métropolitain appartient entièrement à la commune ou à une même entreprise, comme ce peut être le cas pour un réseau de chauffage urbain par exemple, c'est bien un réseau LAN.
Souvent aujourd'hui les réseaux méropolitains utilisent les canaux du réseau internet.

**diapo 8 Réseau local / réseau étendu : le cas des réseaux IoT**

Les réseaux d'objets connectés, ou IoT pour Internet of Things, sont un sujet médiatique qui regroupe des réalités bien différentes. Les équipements de domotiques peuvent y être inclus.

Si les capteurs sont connectés à un réseau privés localisés, comme pour un réseau personnel de domotique, mais l'échelle d'un site industriel, cela reste un réseau local.

Pour se distnguer des objets connectés moqués pour leur aspect gagdet (les brosses à dents connectées par exemple), les fournisseurs de l'industrie utilisent le sigle IIoT Industrial Internet of Things.

**diapo 9 Réseau local / réseau étendu : le cas des réseaux IoT**

Des réseaux longue portée, faible consommation, faible débit, opérés sur le territoire national et européen proposent également de collecter les informations d'objets connectés.

On entre dans la catégorie des réseaux étendus.
Le terme LPWAN (Low Power Wide Area Network) permet de distinguer ces réseaux des réseaux cellulaires, fort débit maais plus gourmands et plus coûteux, dont les antennes partagent souvent les pylônes.

**diapo 10 Réseau de terrain / réseau informatique** 

Un réseau de terrain assure la communication entre le système de contrôle-commande et ses entrées/sorties. 

Il est local, s’étend sur un espace limité et souvent a des contraintes de déterminisme (latence fixe).
Ex : réseau de contrôle commande d’une usine, réseau de bord d’une voiture….

Un réseau informatique assure la communication entre des ordinateurs, des smartphones, des imprimantes..., souvent sur de grandes distances.
Ex : Internet

Sur un système automatisé, qui peut être critique et déterministe, le réseau de supervision est souvent un réseau informatique, pour 2 raisons : 
* Les serveurs de base de données, les PCs de supervision sont des équipements du monde informatique, non compatibles avec les réseaux de terrain,
* L'utilisation d'un réseau informatique permet facilement de router les informations sur Internet et ainsi de faire de la supervision à distance, pour des bâtiments ou usines où il n'y a pas de personnel compétent sur ces systèmes sur place.

## 2. Notions générales sur les réseaux

**diapo 11 Vocabulaire**

Lors d'un échange entre deux machines : 
* Lorsque l'information circule sur un canal dans une seul direction, on parle de __liaison simplex__. (clic)
* Lorsque l'information circule dans les deux sens, sur un même canal, c'est une __liaison half duplex__. (clic)
* Lorsque les informations circulent simulatnément dans les deux sens, c'est une __liaison full duplex__.

Si le canal de transmission transporte 8, 16, 32 ou 64 bits simulatanément, c'est une __transmission parallèle__. Très rapide, très gourmand en cuivre et très exigeant dans la réalisation pour éviter la diaphonie (la perturbation d'une voie par une autre), c'est le mode utilisé entre le processeur et la mémoire.

Les autres échanges utilisent des __transmissions série__, les bits étant envoyées les uns après les autres. La figure sur la droite présente le bus SATA, série, qui remplace les anciens câbles parallèles IDE pour la connexion des disques durs.

Notons que les liaisons vers les cartes SD/MMC utilisent 4 voies en parallèles et l'Ethernet Gigabit transmet 2 bits à la fois.

Parmi les transmissions sans fils, certaines modulations permettent d'envoyer 2, 4 ou 8 bits en parallèle.

Une __transmission synchrone__ est une transmission comprenant l'horloge et les données. Une __transmission asynchrone__ ne comprend que les données, les hrologes devant être identiques du côté émetteur et du côté récepteur, ce qui ne peut être considéré acceptable, à un coût raisonnable, que pour des échanges courts.

Là encore, la scission n'est pas nette entre les deux modes, certains réseaux mélangeant l'horloge avec les données ou prévoyant des mécanismes de resynchronisation.

Sur un réseau, lorsqu'un noeud émet vers tous les autres noeuds, on parle de __diffusion__. La télévision et la radio sont des réseaux à diffusion par exemple. Le terme anglais, très utilisé, est broadcast.
Lorsqu'un noeud émet vers un noeud précis, on parle d'__émission point à point__ ou d'émission unicast.
Enfin, si un noeud envoie un message pour un groupe de noeud, on parle d'__émission multipoint__ et plus fréquemment d'émission multicast.

Le __débit__ est le nombre de bits émis par seconde. Son unité est le bit/s ou l'octet/s (Byte/s en anglais) avec tous leurs multiples kbits/s à Gbit/s. Les exercices suivants amèneront à manipuler ces grandeurs.

La __latence__ est le temps qui s'écoule entre la demande d'émission d'un message et sa réception.

**diapo 12 Topologie**

Lorsque deux machines sont connectées entre elles, on utilise le terme __liaison__. Lorsque plus de 2 machines sont connectées, c'est un __réseau__. Les machines connectées à ce réseau sont des noeuds et la façon dont les noeuds sont interconnectés entre eux se nomme la topologie. Cela ne concerne pas les réseaux sans fil.

Dans l'embarqué, la __topologie en bus__ est privilégiée car elle nécessite moins de câble. Chaque noeud est connecté à un même câble/medium de communication. Cette topologie impose des transmissions half-duplex et une gestion de la prise de parole sur le bus, 2 noeuds ne pouvant parler en même temps.

La __topologie maillée__ est l'interconnexion de tous avec tous ou du moins de plusieurs avec plusieurs. Elle est intéressante pour les noeuds de routage d'un grand réseau car une panne sur un équipement ou un câble coupé n'empêche pas le fonctionnement du réseau.

Compromis entre la simplicité du bus et la robustesse du réseau maillé, la __topologie en anneau__ assure une continuité de fonctionnement si le câble est rompu en un point.

Enfin, la __topologie en étoile__ est une multiplication de liaisons points à points entre un swicth central et des machines. Cette topologie est très gourmande en câble mais permet des liaisons full-duplex entre le switch et chaque machine. C'est la technologie utilisée par l'ethernet commuté.

## 3. Des transmissions essentiellement numériques

**diapo 13 Analogique-Numérique**

Historiquement, avant l'invention du transistor et la révolution du numérique, les transmissions étaient analogiques. Le signal à transmettre était modulé, transmis par des antennes ou via des câbles (téléphonie par exemple) puis démodulé.

En parallèle du développement des réseaux informatiques (donc numériques), la numérisation a gagné l'ensemble des services de communication (la télévision en 2005 avec la TNT, la téléphonie progressivement à partir de  l'arrivée de l'ADSL en 1999).

L'image est captée directement de manière numérique par des cellules CCD, le son est converti en signal numérique avant d'être modulé. A l'arrivée, un convertisseur numérique analogique reconstruit le signal sonore à partir du signal numérique reçu.

La radio reste une des seules transmissions analogiques grand public. La diffusion via la radio numérique terrestre doit démarrer à la fin de l'année 2021.

**diapo 14 Les avantages de la transmission numérique**

Le succès des transmissions numériques est lié à plusieurs de ses avantages :
* La qualité nominale est constante dès que l'on passe au-dessus d'un faible rapport signal sur bruit. Une augemntation du débit permet alors d'augmenter cette qualité nominale, comme ce fut le cas avec l'arrivée de la télévision haute définition.
* L'utilisation d'un canal numérique permet de bénéficier des autres technologies numériques développées pour les réseaux informatiques :
    * Internet, ses protocoles et son infrasctruture, pour diffuser dans le monde entier, sur des PCs ou des smartphones,
    * Le stockage et les mécanismes de réplication de ce stockage,
    * Le chiffrement pour garantir la confidentialité,
    * Et enfin la compression. Si la compression sans perte est indépendante de l'infromation transmise, la compression avec perte dépend de la nature du signal (H264 pour la vidéo et MP3 pour le son par exemple)


## 4. Exercices

## 5. Introduction à Ethernet/TCP/IP

Ce cours de réseaux, conformément au programme du CAPES NSI est essentiellement consacré aux réseaux ethernet/TCP/IP, technologies de l'internet.

Avant de détailler les protocoles de ces réseaux dans les chapitres suivants, voici un rapide aperçu historique et sociétal d'internet pour souligner en quoi les réseaux, au même titre que la machine à vapeur, l'électricité et l'informatique avant eux, bouleversent notre société entrant dans sa 4ème révolution industrielle.

**diapo 17 Bref historique d'Ethernet/TCP/IP**

Ces quelques dates permettent de souligner la très courte histoire d'Internet, excepté si nous la commençons aux origines des télécommunications numériques avec le télégraphe électrique de Samuel Morse en 1840.

En 1945, Hiroshima inaugure l'entrée dans un monde nucléarisé. Les technologies des fusées et des missiles intercontinentaux étant voisines, le passage de Spoutnik au-dessus de l'Amérique fait prendre conscience aux Etats-Unis du haut niveau technologique de l'Union Soviétique. Les Etats Unis investissent alors massivement dans la recherche technologique à des fins militaires et créent l'ARPA (Advanced Research Projects Agency) en 1962.

Un des projets de l'ARPA est le développement d'un réseau résilient pour interconnecter des sites militaires. Des universitaires, dont Joseph Licklider, collaborent au projet et 4 Universités sont connectées dès 1969 sur un réseau nommé ARPAnet.

En 1973 sont posées les bases des protocoles TCP/IP qui s'imposent dans les années 80. Au milieu de cette décénie, le réseau s'émancipe des militaires, l'Europe se connecte et le nom d'Internet commence à apparaître.

En 1987, le protocole DNS simplifie et unifie l'adressage des machines et en 1997, le déploiemnent du protocole HTTP1.1 marque le début du Web. Les réseaux cellulaires rejoignent dès la 2G l'internet et en 2011 la 4G offre un accès fluide à internet sur mobile. En 2018, le rapport annuel de Cisco estimait à 3,9 Milliards le nombre d'utilisateurs d'Internet.

Cette expansion rapide d'Internet a permis le développement des jeunes entreprises Amazon, Google et Facebook.

**diapo 18 Evolution du réseau**

Le rapport annuel de Cisco donne une riche image de l'évolution d'Internet et de ses usages. J'ai choisi de ne présenter que la répartition de l'usage par types de contenus transmis. Le Web, les données et le partage de fichiers utilisent 14% du débit contre 75 % pour la vidéo. On comprend ainsi mieux les demandes des fournisseurs d'accès à Internet vers les riches plateformes de Streaming (Netflix, Disney, Amzon et Google/YouTube) pour qu'elles participent aux financement de l'amélioration de l'infrastructure.

La qualité de service offerte par Internet aujourd'hui (débit, latence, robustesse) permet le développement du partage de ressources avec la location d'infrastructures nuagiques (dans le cloud) aux entreprises. On parle de IaaS (*Infrastructure as a Service*), PaaS *Plateform as a Service* jusqu'à SaaS *Software as a Service* où l'entreprise loue l'usage de logiciels, sans se préoccuper de l'infrastructure, du stockage, de la maintenance et des mises à jours.

Autre exemple de partage de ressources, pour les particuliers, Google Stadia, Shadow, Nvidia proposent sur abonnement un accès à des jeux exécutés à distance sur des serveurs équipés de cartes graphiques performantes. On parle de *Cloud Gaming*.

Evidemment tout cela consomme... GreenPeace dans son rapport *Clicking Clean* de 2017 estime que l'informatique et les réseaux consomment 7% de l'électricité mondiale.

**diapo 19 Un monde connecté**

Ces services s'appuient sur une interconnexion mondiale au débit important. Colonne vertébrale de cette interconnexion, les câbles sous-marins, chacun composés de milliers de fibres optiques, relient les différents continents. Le navire montre la lourde et coûteuse installation de cette infrastructure, à comparer avec l'insolence immatérielle des acteurs de l'Internet mondialisé qui en bénéficient : GAFAM (Google, Amazon, Facebok, Apple, Microsoft) en occident et BATX (Baidu, Alibaba, Tencent, Xiaomi) en Chine.

**diapo 20 Aspect socéital lié à l'évolution des réseaux**

Cette dernière diapositive tente de souligner, en 10 lignes, combien l'avénement des réseaux, des bases de données qu'ils ont permis de constituer et des l'intelligence artificielle qui s'en est nourrie, est au coeur de changements de la société dont les limites ne sont pas encore discernables. L'histoire nous dira si les termes révolution industrielle ou disruption, très à la mode, ne sont pas un peu faible...

* **Au niveau économique**, Les 6 plus grosses capitalisations boursières privées de 2021 ont bâti leur fortune sur les réseaux, le cloud ou les données et s'adressent à des milliards de clients. Tesla, TSMC, fabricant des processeurs de Apple et AMD notamment, et dans une moindre mesure Apple, sont les seuls à centrer leur activité sur une fabrication matérielle.
En 2009, seul Microsoft était dans le TOP 10. Les pétroliers, la grande distibution et la santé y avaient encore une large place.
* **Les réseaux sociaux**, non responsables des contenus qu'ils diffusent, sont contrôlés par des algorithmes visant à générer de l'audience, mettant à mal parfois la vérité et la science et souvent le modèle économique des médias.
* Les réseaux, à travers les vols de données, les cyberattaques ou les diffusions de fausses informations lors des élections, sont devenus le théâtre d'une **cyberguerre** entre les grandes puissances. Le cloisonnement de leur réseau devient un enjeux vital pour les régimes autoritaires, le franchissement de ces cloisons un enjeu pour les autres...
* Les réseaux ont permis la mise en place, à pas forcé par le Covid, du **télétravail** et du télé-enseignement, modifiant les interactions entre les salariés et entre les élèves/étudiants et les enseignants.
* Objectif des Universitaires pères de l'Internet, ce réseau reste un formidable **vecteur de diffusion du savoir**. Wikipedia offre une encyclopédie collaborative de qualité à tous les internautes, en 200 langues, y compris là où il n'y a pas de livres dans les écoles. Lors de la pandémie de Covid 19, l'Organisation Mondiale de la Santé n'a pu que constater sa force de diffusion bien inférieure et s'allier à Wikipedia pour la diffusion d'informations fiables sur le virus. On peut aussi noter que le réseau chiffré TOR, en plus de sa réputation sulfureuse, donne un accès à des journaux de qualité dans les pays où il sont censurés.
* L'accès du savoir à tous modifie égalemnent le **rôle de l'enseignant**. Il n'est plus celui qui possède le savoir mais devient celui qui guide et facilite l'acquisition de ce savoir. Ce sera notre rôle tout au long de ce module.


# TODO
Correction des parties 0.1 à 0.4
