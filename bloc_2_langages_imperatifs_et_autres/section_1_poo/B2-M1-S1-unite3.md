

## Conclusion

La Programmation Orientée Objet rend la conception et la mise en oeuvre de gros projets répartis sur plusieurs développeurs plus aisée, aussi bien lors de la création que pendant la maintenance.

La POO est née avec le langage Smalltalk en 1972, lui-même inspiré de Simula 67 le premier langage à avoir intégré des classes en l'occurence ici les _Record Class_ de Hoare (1965). Depuis nombre de langages de POO ont vu le jour. Les plus célèbres restent : Java et C++. 

Bien que Python ne soit pas **le** langage orienté objet par excellence, il intègre bien tous les concepts importants : de l'encapsulation à l'héritage multiple.

Dans la prochaine section, nous vous proposons un ensemble de quatre vidéos mettant en oeuvre la conception orientée objet pour la réalisation d'un mini jeu d'_escape game_. Ce jeu sera à réaliser entièrement dans le cadre de ce bloc, comme projet.

