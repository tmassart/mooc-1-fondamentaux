# Paradigmes de programmation

Il existe de nombreux paradigmes de programmation, plus de 25 d'après les spécialistes. Ces paradigmes s'appuient sur des concepts de programmation. Un des concepts clés, la base de nombreux paradigmes : l'enregistrement (regroupement de données avec accès direct à chaque donnée).

On peut tout fois, en première simplification considérer deux grands paradigmes que sont la programmation impérative et la programmation déclarative. 

## Programmation Impérative

Le programmeur décrit les étapes à suivre par la machines pour atteindre le résultat.

### Langages procéduraux

- C
- Pascal
- Python

### Langages Objets

- Java
- c++
- Python

La plupart des langages modernes sont multi-paradigmes. Etendre sa connaissance des paradigmes et des langages qui les mettent en oeuvre efficacement est une excellente idée puisque le choix de tel ou tel paradigme est dicté par le problème à résoudre. Abraham Marslow : « Si le seul outil que vous avez est un marteau, vous verrez tout problème comme un clou. ».

## Programation Déclarative

Le programmeur décrit le résultat à obtenir. Les étapes pour y arriver sont confiées à la machine.

Parmi les langages déclaratifs on trouve :

### Les langages fonctionnels 

- Lisp
- Haskell


### Les langages par contraintes

- Prolog III et IV


### Les langages logiques 

- Prolog


## Les techniques de programmation

### La programmation asynchrone

Style de programmation qui permet l'optimisation des cycles d'entrées/sorties (comme par exemple les échanges de requêtes avec un serveur) sans remettre en question la logique du code et en gardant une grande lisibilité.

### La programmation parallèle

Style de programmation qui consiste à utiliser plusieurs processeurs pour effectuer des traitements simultanés. La programmation parallèle nécessite des algorithmes adaptés. Attention une confusion est souvent faite entre programmation _multi-threadings_ c'est-à-dire multi-tâches et la programmation parallèle. Un programme peut lancer plusieurs processus sans pour autant que ceux-ci soient exécutés en parallèle.

### La programmation événementielle

Style de programmation qui prend en compte les événements : les actions de l'utilisateur ou événements internes au programme (chargement d'un fichier) n'interrompt pas le programme mais provoque l'appel à une fonction. Ce mécanisme s'oppose à la programmation séquentielle où l'action avec l'utilisateur interrompt le cours du programme.
