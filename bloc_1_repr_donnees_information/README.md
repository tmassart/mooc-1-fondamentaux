# MOOC1_Fondamentaux
## bloc_1_repr_donnees_information
### section_1_types_valeurs_de_base

#### Fiche donnees_de_base

- 12 vidéos prévues (50')
- scenarios de 11 videos écrits
- reste scénarion de conclusion de la partie (voir comment on fait ça)

##### Contenu 

Valeur et représentation de données de base (int, float, str)
+ images

#### Support écrit

https://owncloud.ulb.ac.be/index.php/s/rg12kwAjlx7ZiYX

Ca compile avec xelatex.

partir du fichier chapitres/02-representation.tex


