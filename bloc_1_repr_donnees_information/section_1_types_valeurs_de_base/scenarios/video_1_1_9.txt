MOOC NSI - fondamentaux
chapitre 1
section 1
video 1_1_9
durée : 7'

Titre : Représentation des caractères

Maintenant que nous avons étudié des techniques pour représenter et
manipuler des nombres en binaire, intéressons-nous à d'autres types
d'informations. Nous commencerons par le texte, c'est-à-dire des
séquences de caractères.

L'idée générale de la représentation (en binaire) des caractères
consiste à faire correspondre à chaque caractère un et un seul nombre
naturel, et à représenter le caractère par la représentation binaire
de ce naturel.

Comme il n'existe pas de manière universelle et naturelle d'associer
un nombre à chaque caractère, il faut fixer une table, qui fait
correspondre chaque caractère à un nombre.

Sous-titre :Codes historiques: Emile Baudot et les téléscripteurs

Le besoin de représenter et transmettre de l'information de manière
mécanique est une préoccupation qui a reçu une première réponse lors
de l'introduction du télégraphe, où le fameux code Morse était utilisé
pour représenter les lettres par une série de traits et de points.

Au dix-neuvième siècle, l'ingénieur français, Jean-Maurice Emile
Baudot invente le premier code pratique permettant de représenter tout
l'alphabet latin (ainsi que des symboles de ponctuation usuels) sur 5
bits.

La cadence à laquelle les caractères pouvaient être transmis par un
téléscripteur était mesurée en bauds, une unité que l'on connait
encore aujourd'hui, bien qu'elle soit tombée en désuétude.

Ces systèmes ont été largement utilisés pour la transmission
d'information à travers les ligne téléphoniques, jusque dans les
années 1980.

Sous-titre Le code ASCII

L'évolution la plus marquante des
développements historiques que nous venons de présenter est le code
ASCII (l'accronyme de  American Standard Code for Information Interchange},
présenté en 1967), qui est encore en usage aujourd'hui.

Il comprend 128 caractères encodés sur 7 bits.

Ce code, bien adapté à la langue anglaise, ne permet pas de
représenter les caractères accentués.

C'est pourquoi plusieurs extensions ont vu le jour, sur 8 bits, où les
128 caractères supplémentaires permettaient d'encoder les caractères
propres à une langue choisie.

Ainsi la norme ISO-8859-1 appelée communément ISO-latin-1 étend la
norme ASCII et permet d'encoder les lettres avec ou sans signe
diacritique (comme un accent, une cédille, des points, crochets, tilde,
rond, etc.) de la plupart ds langues européennes.


Sous-titre Unicode

Plus récemment, le projet Unicode a vu le jour, et s'est donné pour
objectif de créer une norme reprenant la plupart des systèmes
d'écriture utilisés dans le monde, et de leur attribuer un encodage.

La version 13 d'Unicode, publiée en mars 2020 comprend 143 859
caractères.

La norme Unicode associe d'une part une valeur entière à chaque
caractère appelé point de code et généralement dénoté

U+ suivi de 4 à 6 chiffres hexadécimaux (par exemple, U+00C7
correspond au 'Ç' (c cédille en majuscule)

et d'autre part un encodage des caractères parmi plusieurs systèmes
d'encodages.

L'encodage le plus courant est appelé UTF-8: il s'agit d'un encodage à
longueur variable car chaque caractère est encodé sur 1, 2, 3 ou 4
octets.

Tous les caractères
encodés sur 1 octet sont compatibles avec le standard ASCII. Les
encodages sur 2, 3 et 4 octets sont utilisés pour représenter d'autres
caractères, aussi exotiques soient-ils, etcetera.

La norme Unicode est devenue aujourd'hui bien adoptée, notamment par
les sites et navigateurs web ainsi que par le langage Python.




